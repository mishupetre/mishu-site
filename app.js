'use strict';

const path = require('path');
const dotenv = require('dotenv');
const express = require('express');
const bodyParser = require('body-parser');
const compress = require('compression');
const session = require('express-session');
const flash = require('express-flash');
const passport = require('passport');
const mongoose = require('mongoose');
const moment = require('moment');
const shortDateFormat = "Do of MMMM @ h:mmA";

const ENV = process.env.NODE_ENV || 'development';
const PORT = process.env.PORT || 4000;


// Create Express server
let app = express();


// Load environment variables from .env file, where API keys and passwords are configured.
dotenv.load({
  path: '.env'
});


// Connect to MongoDB
mongoose.connect(process.env.MONGODB);
mongoose.connection.on('error', function() {
  console.log('MongoDB Connection Error. Please make sure that MongoDB is running.');
  process.exit(1);
});


// Express configuration
app.use('/static', express.static(path.join(__dirname + '/static')));
app.set('view engine', 'jade');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));
app.set('views', path.join(__dirname, 'views'));
app.use(compress());
app.use(session({
  name: 'session',
  resave: true,
  saveUninitialized: true,
  secret: process.env.SESSION_SECRET
}));
app.use(flash());

app.use(passport.initialize());
app.use(passport.session());

app.use(function(req, res, next) {
  res.locals.user = req.user;
  next();
});

app.locals.moment = moment;
app.locals.shortDateFormat = shortDateFormat;

const publicController = require('./controllers/public');

app.get('/', publicController.getIndex);
app.get('/blog', publicController.getArticles);
app.get('/blog/articles/:id', publicController.getArticle);

app.get('/archive', publicController.getArchive);

app.get('/about-me', publicController.getAbout);
app.get('/contact', publicController.getContact);


const passportConfig = require('./config/passport');
const adminController = require('./controllers/admin');

app.get('/admin', adminController.getIndex);
app.get('/admin/login', adminController.getLogin);
app.get('/admin/signup', adminController.getSignup);
app.post('/admin/login', adminController.postLogin);
app.post('/admin/signup', adminController.postSignup);
app.get('/admin/logout', adminController.getLogout);

app.get('/admin/articles', passportConfig.isAuthenticated, adminController.getArticles);
app.get('/admin/articles/add', passportConfig.isAuthenticated, adminController.getAddArticle);
app.post('/admin/articles/add', passportConfig.isAuthenticated, adminController.postAddArticle);
app.get('/admin/articles/:id', passportConfig.isAuthenticated, adminController.getArticle);
app.get('/admin/articles/:id/edit', passportConfig.isAuthenticated, adminController.getEditArticle);
app.post('/admin/articles/:id/edit', passportConfig.isAuthenticated, adminController.postEditArticle);
app.get('/admin/articles/:id/delete', passportConfig.isAuthenticated, adminController.getDeleteArticle);


// Error handlers for admin area
app.use('/admin', (req, res, next) => {
  res.status(404).send('not found');
});

app.use('/admin', (err, req, res, next) => {
  if (ENV === 'development') {
    console.log(err.stack);
    return res.status(500).send(err.stack);
  }

  res.status(500).send('oops! something broke');
});

// Error handlers for public site
app.use((req, res, next) => {
  res.status(404).render('public/404');
});

app.use((err, req, res, next) => {
  if (ENV === 'development') {
    console.log(err.stack);
    return res.status(500).send(err.stack);
  }

  res.status(500).send('oops! something broke');
});


// Start Express server
app.listen(PORT, function() {
  console.log(`Server listening on port ${PORT}...`);
});