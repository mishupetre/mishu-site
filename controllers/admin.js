'use strict';
var passport = require('passport');
var User = require('../models/User');
var Article = require('../models/Article');

exports.getIndex = function(req, res) {
  res.redirect('/admin/login');
}

exports.getLogin = function(req, res) {
  if (req.user) {
    return res.redirect('/admin/articles');
  } else {
    res.render('admin/login');
  }
}

exports.postLogin = function(req, res, next) {
  passport.authenticate('local', function(err, user, info) {
    if (err) {
      return next(err);
    }

    if (!user) {
      req.flash('errors', info);
      return res.redirect('/admin/login');
    }

    req.logIn(user, function(err) {
      if (err) {
        return next(err);
      }
      req.flash('success', {
        msg: 'Welcome, you\'ve logged in successfully.'
      });
      res.redirect('/admin/articles');
    });
  })(req, res, next);
}

exports.getSignup = function(req, res) {
  if (req.user) {
    return res.redirect('/admin/articles');
  } else {
    res.render('admin/signup');
  }
}

exports.postSignup = function(req, res) {
  var user = new User({
    name: req.body.name,
    email: req.body.email,
    password: req.body.password
  });

  User.findOne({
    email: req.body.email
  }, function(err, existingUser) {
    if (existingUser) {
      req.flash('errors', {
        msg: 'An user with that email already exists.'
      });
      return res.redirect('/admin/signup');
    }
    user.save(function(err) {
      if (err) {
        return next(err);
      }
      req.logIn(user, function(err) {
        if (err) {
          return next(err);
        }
        req.flash('success', {
          msg: 'You\'ve logged in successfully!'
        })
        res.redirect('/admin/articles');
      });
    });
  });
}

exports.getArticles = function(req, res) {
  Article
    .find({})
    .sort('-date')
    .exec(function(err, articles) {
      if (err) {
        next(err);
      }
      res.render('admin/articles/list', {
        articles: articles
      });
    });
}

exports.getArticle = function(req, res) {
  Article.findById(req.params.id, function(err, article) {
    if (err) {
      next(err);
      return res.send('Failed to retrieve the requested article.');
    }
    res.render('admin/articles/view', {
      article: article
    });
  });
}

exports.getAddArticle = function(req, res) {
  res.render('admin/articles/form', {
    article: {}
  });
}

exports.postAddArticle = function(req, res) {
  var article = new Article({
    title: req.body.title,
    secondaryTitle: req.body.secondaryTitle,
    content: req.body.content,
    isPublished: req.body.isPublished,
    author: req.user.name
  });

  article.save(function(err) {
    if (err) {
      console.log(err);
      return res.send('Something happened when trying to save your new article.')
    }

    res.redirect('/admin/articles');
  });
}

exports.getEditArticle = function(req, res) {
  Article.findById(req.params.id, function(err, article) {
    if (err) {
      console.log(err);
      return res.send('Failed to retrieve the requested article.');
    }
    res.render('admin/articles/form', {
      article: article
    });
  });
}

exports.postEditArticle = function(req, res) {
  Article.findByIdAndUpdate(req.params.id, {
    title: req.body.title,
    secondaryTitle: req.body.secondaryTitle,
    content: req.body.content,
    isPublished: req.body.isPublished,
    author: req.user.name,
    modified: Date.now()
  }, {
    new: true
  }, function(err, article) {
    if (err) {
      console.log(err);
      res.send('Failed to update article ' + article.title);
    }
    res.redirect('back');
  });
}

exports.getDeleteArticle = function(req, res, next) {
  Article.findByIdAndRemove(req.params.id, function(err) {
    if (err) {
      return next(err);
    }
    req.flash('info', {
      msg: 'Article deleted!'
    });
    res.redirect('/admin/articles');
  })
}

exports.getLogout = function(req, res) {
  req.logout();
  res.redirect('/admin/login');
}