'use strict';

const Article = require('../models/Article');

exports.getIndex = function(req, res) {
  res.redirect('/blog');
}

exports.getArticles = function(req, res) {
  Article
    .find({
      isPublished: true
    })
    .sort('-date')
    .limit(3)
    .exec(function(err, articles) {
      if (err) {
        return next(err);
      }
      res.render('public/blog/list', {
        articles: articles
      });
    });
}

exports.getArticle = function(req, res) {
  Article.findById(req.params.id, function(err, article) {
    if (err) {
      next(err);
      return res.send('Failed to retrieve the requested article.');
    }

    res.render('public/blog/article', {
      article: article
    });
  })
}

exports.getArchive = function(req, res) {
  Article
    .find({
      isPublished: true
    })
    .sort('-date')
    .exec(function(err, articles) {
      if (err) {
        return next(err);
      }
      res.render('public/blog/archive', {
        articles: articles
      });
    })
}

exports.getAbout = function(req, res) {
  res.render('public/about-me');
}

exports.getContact = function(req, res) {
  res.render('public/contact');
}